import { Button } from "@/components/ui/button";
import { Avatar } from "@/components/ui/avatar";
import Link from "next/link";
import { Badge } from "@/components/ui/badge";
import { CardContent, Card } from "@/components/ui/card";
import { Textarea } from "@/components/ui/textarea";
import { Home } from "@/components/home";

export default function Component() {
  return (
    <main key="1" className="flex flex-col h-screen p-4 bg-gray-50">
      <header className="flex items-center justify-between py-2">
        <h1 className="text-2xl font-bold">AI Chatbot</h1>
        <div className="flex items-center space-x-4">
          <Button variant="outline">Export to JSON</Button>
          <Button variant="outline">Export to Markdown</Button>
        </div>
      </header>
      <div className="flex flex-1 overflow-hidden">
        <aside className="flex flex-col w-64 p-2 space-y-4 bg-white border-r overflow-y-auto">
          <h2 className="text-lg font-semibold">Conversations</h2>
          <Link
            className="flex items-center p-2 rounded hover:bg-gray-100"
            href="#"
          >
            <Avatar className="mr-2" />
            <span>Conversation 1</span>
          </Link>
          <Link
            className="flex items-center p-2 rounded hover:bg-gray-100"
            href="#"
          >
            <Avatar className="mr-2" />
            <span>Conversation 2</span>
          </Link>
          <Link
            className="flex items-center p-2 rounded hover:bg-gray-100"
            href="#"
          >
            <Avatar className="mr-2" />
            <span>Conversation 3</span>
          </Link>
        </aside>
        <div className="flex flex-col flex-1 p-4 space-y-4">
          <Card className="flex-1 overflow-y-auto">
            <CardContent className="space-y-4">
              <div className="flex items-start space-x-2">
                <Avatar className="mr-2" />
                <div className="text-sm bg-blue-200 rounded-md p-2">
                  <Badge className="mb-1">AI</Badge>
                  <p>Hello, how can I assist you today?</p>
                </div>
              </div>
              <div className="flex items-start space-x-2">
                <Avatar className="mr-2" />
                <div className="text-sm bg-green-200 rounded-md p-2">
                  <Badge className="mb-1">User</Badge>
                  <p>I need help with my account.</p>
                </div>
              </div>
            </CardContent>
          </Card>
          <div className="flex items-center space-x-2">
            <Textarea
              className="flex-1"
              placeholder="Type your message here."
            />
            <Button>Send Message</Button>
          </div>
        </div>
      </div>
    </main>
  );
}
